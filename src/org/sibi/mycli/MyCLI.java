package org.sibi.mycli;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintStream;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Set;

public class MyCLI {

	private PrintStream mOutput;
	private boolean mRUN = false;
	private final int mVerMajor = 0;
	private final int mVerMinor = 5;

	private HashMap<String, Container> registeredCommands = new HashMap<>();
	private HashMap<String, String> registeredAliases = new HashMap<>();

	public PrintStream getStdOut() {
		return mOutput;
	}

	/**
	 * Print strings on set output. Newline is set.
	 * @param s String to print
	 */
	public void println(String s) {
		mOutput.println(s);
		mOutput.print("\n%> ");
	}

	/**
	 * Print strings on set output. NO newline is set.
	 * @param s String to print
	 */
	public void print(String s) {
		mOutput.print(s);
	}

	private class Container {
		private String name;
		private String description;
		private ICommandWrapper commandWrapper;

		public Container(String n, String d, ICommandWrapper c) {
			name = n;
			description = d;
			commandWrapper = c;
		}

		public String getName() {
			return name;
		}
		public String getDescription() {
			return description;
		}
		public ICommandWrapper getCommandWrapper() {
			return commandWrapper;
		}
	}

	/**
	 * creates the default CLI. Equivalent to MyCLI(System.out)
	 */
	public MyCLI() {
		this(System.out);
	}

	/**
	 * creates a CLI with custom output stream
	 * @param output the output tu use
	 */
	public MyCLI(PrintStream output) {
		this.mOutput = output;

		registerCommand("help", "Prints help screen and lists all commands. Args: none", helpCommand, "?");
		registerCommand("exit", "Ends the programm. Args: none", exitCommand, "x", "q", "quit");
	}

	/**
	 * same as start(true)
	 * @throws InterruptedException
	 */
	public void start() throws InterruptedException {
		start(true, false, false, null, -1);
	}

	/**
	 * start the command line interpreter
	 * @param blocking returns immediately if false, returns after exit command if true
	 * @throws InterruptedException
	 */
	public void start(boolean blocking, boolean daemon, boolean connect, String host, int port) throws InterruptedException {
		mRUN = true;

		if (daemon && !connect) {
			cliThread = new Thread(daemonRunnable);
			try {
				ces = new CommandExchangeServer(port);
			} catch (IOException e) {
				e.printStackTrace();
				return;
			}
		} else if (!daemon && connect) {
			cliThread = new Thread(connectClientRunnable);
			cec = new CommandExchangeClient(host, port);
		} else { /* both false or both true */
			cliThread = new Thread(simpleCLIRunnable);
		}

		cliThread.start();
		if (blocking)
			cliThread.join();
	}

	public BufferedReader getBufferedReader() {
		if (ces != null)
			return ces.getBufferedReader();
		else
			return br;
	}

	BufferedReader br = null;
	CommandExchangeServer ces = null;
	CommandExchangeClient cec = null;

	private Runnable simpleCLIRunnable = new Runnable() {
		@Override
		public void run() {
			String command = null;
			br = new BufferedReader(new InputStreamReader(System.in));
			mOutput.println("Welcome to sibi CLI v " + mVerMajor + "." + mVerMinor + ".");
			mOutput.println("type or \"help\" or \"?\" for a list of commands.");
			mOutput.println("type \"exit\" or \"quit\" to quit.");
			mOutput.println("Have a nice day :)");
			while (mRUN) {
				mOutput.print("\n%> ");
				try {
					command = br.readLine();
				} catch (IOException e) {
					e.printStackTrace();
					mOutput.println("Dafuq? What did you do..?");
				}
				evaluate(command);
			}
		}
	};
	private Runnable daemonRunnable = new Runnable() {
		@Override
		public void run() {
			mOutput = ces.getOutput();
			String command = null;

			mOutput.println("Welcome to sibi CLI v " + mVerMajor + "." + mVerMinor + ".");
			mOutput.println("type or \"help\" or \"?\" for a list of commands.");
			mOutput.println("type \"exit\" to end the client and \"quit\" to end the server.");
			mOutput.println("Have a nice day :)");
			while (mRUN) {
				mOutput.print("\n%> ");
				command = ces.getCommand();
				if (command == null)
					continue;
				mOutput = ces.getOutput();
				evaluate(command);
			}
		}
	};
	private Runnable connectClientRunnable = new Runnable() {

		@Override
		public void run() {

			String command = null;
			br = new BufferedReader(new InputStreamReader(System.in));
			mOutput.println("Welcome to sibi CLI v " + mVerMajor + "." + mVerMinor + ".");
			mOutput.println("type or \"help\" or \"?\" for a list of commands.");
			mOutput.println("type \"exit\" to quit.");
			mOutput.println("Have a nice day :)");
			while (mRUN) {
				mOutput.print("\n%> ");
				try {
					command = br.readLine();
				} catch (IOException e) {
					e.printStackTrace();
					mOutput.println("Dafuq? What did you do..?");
				}
				if ("exit".equals(command)) {
					mRUN = false;
					cec.stop();
				} else {
					/* Exit is supposed to make only the client stop */
					cec.sendCommand(command);
				}
			}
		}
	};
	Thread cliThread = null;

	private boolean evaluate(String commandAndParameters) {
		if (commandAndParameters == null || commandAndParameters.length() == 0)
			return false;

		String[] parts = commandAndParameters.split(" ");
		if (parts.length < 1)
			return false;

		String command = parts[0];

		String aCommand = registeredAliases.get(command);

		if (aCommand != null)
			command = aCommand;

		ArrayList<String> args = null;
		if (parts.length > 1) {
			args = new ArrayList<>(parts.length - 1);
			for (int i = 1; i < parts.length; i++)  {
				args.add(parts[i]);
			}
		}

		Container cc = registeredCommands.get(command);

		if (cc == null) {
			ArrayList<String> guesses = guessCommand(command);
			if (guesses.size() == 0) {
				mOutput.println("Unknown command: " + command);
				return false;
			} else if (guesses.size() > 1) {
				println("Possible Commands: ");
				for (String c : guesses) {
					print(c + " ");
				}
				return false;
			} else if (guesses.size() == 1) {
				cc = registeredCommands.get(guesses.get(0));
				println(guesses.get(0));
			}
		}
		if ( cc.getCommandWrapper() == null ) {
			mOutput.println("command registered but not assigned: " + command);
			return false;
		} else {
			cc.getCommandWrapper().invoke(args == null ? null : args.toArray());
			return true;
		}

	}


	/* simple auto completion approach */
	private ArrayList<String> guessCommand(String command) {
		Set<String> commandKeys = registeredCommands.keySet();
		ArrayList<String> possibleCommands = new ArrayList<>();

		int l = command.length();

		for (String commandKey : commandKeys) {
			if (l > commandKey.length())
				continue;
			if (command.equals(commandKey.substring(0, l))) {
				possibleCommands.add(commandKey);
			}
		}
		return possibleCommands;
	}

	public boolean injectCommand(String command, Object[] args) {
		String cString;

		if (args == null || args.length == 0) {
			cString = command;
		} else {
			StringBuilder sb = new StringBuilder();
			sb.append(command);
			for (int i = 0 ; i < args.length; i++) {
				sb.append(" ");
				sb.append(args[i]);
			}
			cString = sb.toString();
		}
		mOutput.println("<injecting> " + cString);
		return evaluate(cString);
	}

	/**
	 * This is where the magic happens. Register commands here. Command supersedes Alias. If this happens,
	 * a warning is printed.
	 * @param name the command name
	 * @param description the command's description. Should contain thorough details about parameters
	 * @param commandWrapper contains the function to be invoked
	 * @param aliases zero or more aliases for the command name (e.g. alias "?" for command "help"
	 * @return true since right now there is nothin to fail...
	 */
	public boolean registerCommand(String name, String description, ICommandWrapper commandWrapper, String... aliases) {

		String oldAlias = registeredAliases.get("name");
		if (oldAlias != null) {
			mOutput.println("Alias " + name + " for " + oldAlias + " gets overwritten by new command " + name + ".");
			registeredAliases.remove(name);
		}

		Container c = new Container(name, description, commandWrapper);
		registeredCommands.put(name, c);

		for (String alias : aliases) {
			registeredAliases.put(alias, name);
		}

		return true;
	}

	/* the command function for the help command */
	private final ICommandWrapper helpCommand = new ICommandWrapper() {
		@Override
		public boolean invoke(Object[] parameters) {
			mOutput.println("Help Screen. All registered commands.");

			for (Container c : registeredCommands.values()) {
				mOutput.println("\ncommand name : " + c.getName());
				mOutput.println("description  : " + c.getDescription());
			}

			return true;
		}
	};

	/* the command function for the exit command */
	private final ICommandWrapper exitCommand = new ICommandWrapper() {

		@Override
		public boolean invoke(Object[] parameters) {
			mRUN = false;
			return true;
		}
	};

}
