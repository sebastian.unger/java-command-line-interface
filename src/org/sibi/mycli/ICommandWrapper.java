package org.sibi.mycli;

public interface ICommandWrapper {
	/**
	 * the code to be invoked upon running a certain command
	 * @param parameters parameters for the function. You should take great care to properly describe them when registering the command.
	 * @return
	 */
	boolean invoke(Object[] parameters);
}
