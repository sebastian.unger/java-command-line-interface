package org.sibi.mycli;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintStream;
import java.net.ServerSocket;
import java.net.Socket;
import java.net.UnknownHostException;

public class CommandExchangeServer {

	ServerSocket serverSocket = null;
	Socket socket = null;
	Socket resultSocket = null;
	PrintStream mOutput = null;
	boolean connected = false;

	public PrintStream getOutput() {
		return mOutput;
	}

	public CommandExchangeServer(int port) throws IOException {
		serverSocket = new ServerSocket(port);
		mOutput = new PrintStream("./console.log");
	}

	BufferedReader bufferedReader = null;
	public BufferedReader getBufferedReader() {
		return bufferedReader;
	}

	public String getCommand() {

		if (!connected) {
			try {
				socket = serverSocket.accept();
			} catch (IOException e) {
				e.printStackTrace();
			}
			connected = true;
			mOutput.println("Client connected!");

			String clientAddress = socket.getInetAddress().getHostAddress();
			int clientPort = socket.getLocalPort() + 1;

			mOutput.println("Assuming client Address " + clientAddress + "(" + socket.getInetAddress() + ") : " + clientPort);
			mOutput.flush();
			mOutput.print("Connecting to result socket ");
			while (resultSocket == null) {
				try {
					resultSocket = new Socket(clientAddress, clientPort);
				} catch (UnknownHostException e1) {
					mOutput.print(".");
				} catch (IOException e1) {
					mOutput.print(".");
				} finally {
					if (resultSocket != null) {
						mOutput.println(" [connected]");
					}
				}
			}

			try {
				mOutput.flush();
				mOutput.close();
				mOutput = new PrintStream(resultSocket.getOutputStream());
			} catch (IOException e) {
				e.printStackTrace();
				resetServer();
			}
		}

		try {
			bufferedReader = new BufferedReader(new InputStreamReader(socket.getInputStream()));
		} catch (IOException e) {
			e.printStackTrace();
			resetServer();
		}

		String command = null;
		try {
			command = bufferedReader.readLine();
		} catch (IOException e) {
			e.printStackTrace();
			resetServer();
		}

		if (command == null) {
			resetServer();
		}

		return command;
	}

	private void resetServer() {
		if (socket != null) {
			try {
				socket.close();
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			socket = null;
		}

		if (resultSocket != null) {
			try {
				resultSocket.close();
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			resultSocket = null;
		}

		try {
			mOutput = new PrintStream("./console.log");
			mOutput.println("Client disconnected. Server reset.");
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		bufferedReader = null;
		connected = false;
	}

}
