package org.sibi.mycli;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.io.PrintWriter;
import java.net.ServerSocket;
import java.net.Socket;
import java.net.UnknownHostException;

public class CommandExchangeClient {
	Socket writeSocket = null;
	ServerSocket readServerSocket = null;

	Thread readThread = null;

	PrintWriter pw = null;
	BufferedReader br = null;

	boolean mRUN = true;

	public void stop() {
		mRUN = false;
		try {
			br.close();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		try {
			readServerSocket.close();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		try {
			writeSocket.close();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		try {
			readThread.join();
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	public CommandExchangeClient(String address, int port) {
		System.out.print("Connecting ");
		while (writeSocket == null) {
			try {
				writeSocket = new Socket(address, port);
			} catch (UnknownHostException e) {
				System.out.print(".");
				writeSocket = null;
			} catch (IOException e) {
				System.out.print(".");
				writeSocket = null;
			} finally {
				if (writeSocket == null) {
					try {
						Thread.sleep(1000);
					} catch (InterruptedException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
				} else {
					System.out.println(" [connected]");
				}

			}
		}
		try {
			readServerSocket = new ServerSocket(port+1);
			pw = new PrintWriter(new OutputStreamWriter(writeSocket.getOutputStream()));

			readThread = new Thread(readThreadRunnable);
			readThread.start();

		} catch (UnknownHostException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	public void sendCommand(String command) {
		if (command == null || command.isEmpty())
			return;
		pw.println(command);
		pw.flush();
	}

	Runnable readThreadRunnable = new Runnable() {

		@Override
		public void run() {
			Socket s = null;
			try {
				s = readServerSocket.accept();
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			try {
				br = new BufferedReader(new InputStreamReader(s.getInputStream()));
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}

			char[] buffer = null;
			while (mRUN) {
				buffer = new char[20000];
				try {
					br.read(buffer);
				} catch (IOException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				String result = String.copyValueOf(buffer);

				System.out.print(result);

			}
		}
	};
}
